from selenium.webdriver.common.keys import Keys

from .base import FunctionalTest

class LayoutAndStylingTest(FunctionalTest):

    def test_layout_and_styling(self):
        # Edith goes to the home page
        self.browser.get(self.live_server_url)
        #self.browser.set_window_size(1024, 768)

        # She notices the input box is nicely centered.
        half_of_screen_size = self.browser.get_window_size()['width']/2
        inputbox = self.get_item_input_box()
        self.assertAlmostEqual(
            inputbox.location['x'] + inputbox.size['width'] /2,
            half_of_screen_size, delta=10
        )

        # She starts a new list and sees the input is nicely
        # centered there too
        self.add_list_item('testing')
        inputbox = self.get_item_input_box()
        self.assertAlmostEqual(
            inputbox.location['x'] + inputbox.size['width'] / 2,
            half_of_screen_size, delta=10
        )

